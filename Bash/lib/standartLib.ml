open Ast2
open Outside

(*----------------------*)

let hash_table_list h = Hashtbl.fold (fun k v acc -> (k, v) :: acc) h []

let run_function name hash_global hash_lockal =
  (*let _ = print_string ("\nexecute: (" ^ name ^ ")\n") in*)
  let pre = "" in
  let post = " > tempOut.txt" in
  let rec list_builder i =
    if Hashtbl.mem hash_lockal (string_of_int i) then
      let temp =
        match (Hashtbl.find hash_lockal (string_of_int i)).sig_value with
        | Value (_, str) -> str in
      temp :: list_builder (i + 1)
    else [] in
  let args = String.concat " " (list_builder 1) in
  (*let _ =
    print_string ("\nexecute script: (" ^ pre ^ name ^ " " ^ args ^ ")\n") in*)
  let status = Sys.command (pre ^ name ^ " " ^ args ^ post) in
  let result = Outside.str_lst_from_file "tempOut.txt" in
  match result with [] -> None | lst -> Some lst

let echo_function hash_global hash_lockal =
  let rec list_builder i =
    if Hashtbl.mem hash_lockal (string_of_int i) then
      let temp =
        match (Hashtbl.find hash_lockal (string_of_int i)).sig_value with
        | Value (_, str) -> str in
      temp :: list_builder (i + 1)
    else [] in
  let result = list_builder 1 in
  match result with [""] -> None | _ :: _ -> Some result | _ -> None
