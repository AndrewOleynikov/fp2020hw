  $ (cd ../../../../default && demos/demoFirst.exe)
  
  //////BCmd+BCommand TEST////////////
  funcFactor () 
  {
    facto=1;
    count=$1;
    while (($count > 0))
     do 
      facto=$(( $facto * $count )); count=$(( $count - 1 ))
    done ; 
    echo $facto 
  }
  funcFactor 4
   <-> 
  BConv(DeclFunct(Method("funcFactor" , Pipline(Dpoint, Expression(Eqal(SimpleVari("facto") , Word(WString(Int(1)))::[])) , Pipline(Dpoint, Expression(Eqal(SimpleVari("count") , Word(WString(Variable(SimpleVari("1"))))::[])) , Pipline(Dpoint, While(GreatAr(Container(Variable(SimpleVari("count"))) , Container(Int(0))) , Pipline(Dpoint, Expression(Eqal(SimpleVari("facto") , Subarifm(Multi(Container(Variable(SimpleVari("facto"))) , Container(Variable(SimpleVari("count")))))::[])) , SigPipe(Expression(Eqal(SimpleVari("count") , Subarifm(Minus(Container(Variable(SimpleVari("count"))) , Container(Int(1))))::[]))))) , SigPipe(Expression(CallFunction("echo" , Word(WString(Variable(SimpleVari("facto"))))::[] , None )))))))) , BSigCmd(PipeConv(SigPipe(Expression(CallFunction("funcFactor" , Word(WString(Int(4)))::[] , None ))))))
  //////WORD TEST////////////
  ssss{${sds[1]},sss}dsdsd = WStringCont(String("ssss") , WbrExpCont(WString(Variable(Braces(ArrayVari("sds", Container(Int(1))))))::WString(String("sss"))::[] , WString(String("dsdsd"))))
  //////PIPECONV TEST////////////
  foreach i ( 1 2 3 4 ) local s=$i ; echo $i end
   <-> 
  SigPipe(Expression(CallFunction("foreach" , Word(WString(String("i")))::[] , None )))
