open Bash_lib.Parser

(*funcFactor ()\n\
   {\n\
   facto=1 ;\n\
   count=$1 ;\n\
   while (( $count > 0 ))\n\
   do\n\
   facto=$(($facto*$count)) ; count=$(($count-1))\n\
   done ; echo $facto\n\
   }\n\n\
   funcFactor 4*)
let pre_print = print_string "\n//////BCmd+BCommand TEST////////////\n"

let (str_bash : string) =
  "funcFactor () \n\
   {\n\
  \  facto=1;\n\
  \  count=$1;\n\
  \  while (($count > 0))\n\
  \   do \n\
  \    facto=$(( $facto * $count )); count=$(( $count - 1 ))\n\
  \  done ; \n\
  \  echo $facto \n\
   }\n\
   funcFactor 4"

(* "funcFactor ()\n\
   {\n\
   facto=1 ;\n\
   count=$1 ;\n\
   while (( $count > 0 ))\n\
   do\n\
   facto=$(($facto*$count)) ; count=$(($count-1))\n\
   done ; echo $facto\n\
   }\n\n\
   funcFactor 4"
*)
(* "result=0\n\
   \  even () {\n\
    local temp=$1;\n\
   \      if (( $temp == 0 )) then\n\
    echo tryyy\n\
    else\n\n\
   \   if (( $temp < 0 )) then\n\
   \        echo nil\n\
   \      else\n\
   \      echo $(odd $(($temp - 1)))\n\
   \      fi\n\n\
   \   fi\n\
    }\n\
    odd () {\n\
    local temp=$1;\n\
   \      if (( $temp == 1 )) then\n\
    echo tryyy\n\
    else\n\n\
   \   if (( $temp < 0 )) then\n\
   \        echo nil\n\
   \      else\n\
   \      echo $(odd $(($temp - 1)))\n\
   \      fi\n\n\
   \   fi\n\
    }\n\
    echo $(even 2)\n\
   \         "
*)
let start_print = print_string (str_bash ^ "\n <-> \n")

let test_result =
  print_string (option_bCmdConv_string (apply pars_bComdConv str_bash))

(*-------------------*)

let _ = print_string "\n//////WORD TEST////////////\n"
let (str_word : string) = "ssss{${sds[1]},sss}dsdsd"
let _ = print_string (str_word ^ " = ")
let _ = print_string (option_word_string (apply pars_word str_word))

(*------------------*)

let _ = print_string "\n//////PIPECONV TEST////////////\n"
let (str_pipe : string) = "foreach i ( 1 2 3 4 ) local s=$i ; echo $i end"
let _ = print_string (str_pipe ^ "\n <-> \n")
let _ = print_string (option_pipeConveyr_string (apply pars_pipeConv str_pipe))
